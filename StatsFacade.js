(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.StatsFacade = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var StatsFacade = require("./src/StatsFacade.js");

module.exports = StatsFacade;

},{"./src/StatsFacade.js":9}],2:[function(require,module,exports){
var RecentGamesModel = require("./RecentGamesModel.js");
var PerformanceModel = require("./PerformanceModel.js");
var MostPlayedChampionModel = require("./MostPlayedChampionModel.js");
var MultiKillHistoryModel = require("./MultiKillHistoryModel.js");
var RecentAchievementsModel = require("./RecentAchievementsModel.js");

function BroadcasterModel(emitter) {
   this.emitter = emitter;

   this.summonerName = "";

   this.medals = {};
   this.record = {};

   this.recentGames = new RecentGamesModel(emitter);
   this.performanceModel = new PerformanceModel();
   this.mostPlayedChampionsModel = new MostPlayedChampionModel();
   this.multiKillHistoryModel = new MultiKillHistoryModel();

   this.recentAchievementsModel = new RecentAchievementsModel();
}

BroadcasterModel.prototype.getPerformanceModel = function() {
   return this.performanceModel;
};

BroadcasterModel.prototype.getMostPlayedChampionsModel = function() {
   return this.mostPlayedChampionsModel;
};

BroadcasterModel.prototype.getMultiKillHistoryModel = function () {
   return this.multiKillHistoryModel;
};

BroadcasterModel.prototype.getRecentAchievementsModel = function() {
   return this.recentAchievementsModel;
};

BroadcasterModel.prototype.getRecentGamesModel = function() {
   return this.recentGames;
};

BroadcasterModel.prototype.changeData = function(recentGames, medals, record ) {

   if( recentGames.game )
      recentGames = recentGames.game;

   this.summonerName = recentGames[0].summoner_name;
   var summonerName = this.summonerName;

   this.recentGames.changeData( summonerName, recentGames );
   this.performanceModel.changeData( summonerName, record );
   this.mostPlayedChampionsModel.changeData( summonerName, medals.topchamp );
   this.multiKillHistoryModel.changeData( summonerName, medals.multikill );
   this.recentAchievementsModel.changeData( summonerName, medals.firsts );

   this.uuid = guid();

   this.emitter.emit("BROADCASTER_CHANGED", {
      "summoner_name": summonerName,
      "recent_games": recentGames,
      "medals": medals,
      "record": record
   })
};

BroadcasterModel.prototype.getAggregateId = function() {
   return this.uuid;
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

module.exports = BroadcasterModel;

},{"./MostPlayedChampionModel.js":3,"./MultiKillHistoryModel.js":4,"./PerformanceModel.js":6,"./RecentAchievementsModel.js":7,"./RecentGamesModel.js":8}],3:[function(require,module,exports){
function MostPlayedChampionModel() {
//   this.topchamp = topchamp;
//   this.topchamp["summonerName"] = summonerName;
   this.callback = null;
}

MostPlayedChampionModel.prototype.changeData = function(summonerName, topchamp) {
   this.topchamp = topchamp;
   this.topchamp["summonerName"] = summonerName;

   if( this.callback )
      this.callback( this.topchamp );
}

MostPlayedChampionModel.prototype.getData = function() {
   return this.topchamp;
}

MostPlayedChampionModel.prototype.registerModelUpdatedCallback = function(callback) {
   this.callback = callback;
};

module.exports = MostPlayedChampionModel;

},{}],4:[function(require,module,exports){
function MultiKillHistoryModel(summonerName, multikill) {
//   this.multikill = multikill;
//   this.multikill["summonerName"] = summonerName;
   this.callback = null;
}

MultiKillHistoryModel.prototype.changeData = function(summonerName, multikill) {
   this.multikill = multikill;
   this.multikill["summonerName"] = summonerName;

   if( this.callback )
      this.callback( this.multikill );
}

MultiKillHistoryModel.prototype.getData = function() {
   return this.multikill;
}

MultiKillHistoryModel.prototype.registerModelUpdatedCallback = function(callback) {
   this.callback = callback;
};

module.exports = MultiKillHistoryModel;

},{}],5:[function(require,module,exports){
function ObjectivesModel( data, timeLength ) {
   this.data = data;
   this.timeLength = timeLength;
   this.callback = null;
}

ObjectivesModel.prototype.changeData = function( data, timeLength ){

   this.data = data;
   this.timeLength = timeLength;

   if( this.callback )
      this.callback( {
         data: this.data,
         time_length: this.timeLength
      });
}

ObjectivesModel.prototype.changeGame = function( data, timeLength ){

   this.data = data;
   this.timeLength = timeLength;

   if( this.callback )
      this.callback( {
         data: this.data,
         time_length: this.timeLength
      });
}

ObjectivesModel.prototype.registerModelUpdatedCallback = function(callback) {
   this.callback = callback;
};

ObjectivesModel.prototype.getData = function() {
   return {
      data: this.data,
      time_length: this.timeLength
   }
};

module.exports = ObjectivesModel;


},{}],6:[function(require,module,exports){
function PerformanceModel(summonerName, record) {
   //this.record = record;
   //this.record["summonerName"] = summonerName;
   this.callback = null;
}

PerformanceModel.prototype.changeData = function(summonerName, record) {
   this.record = record;
   this.record["summonerName"] = summonerName;

   if( this.callback )
      this.callback( this.record );
}

PerformanceModel.prototype.getData = function() {
   return this.record;
}

PerformanceModel.prototype.registerModelUpdatedCallback = function(callback) {
   this.callback = callback;
};

module.exports = PerformanceModel;

},{}],7:[function(require,module,exports){
function RecentAchievementsModel(summonerName, firsts) {
//   this.firsts = firsts;
//   this.firsts["summonerName"] = summonerName;
   this.callback = null;
}

RecentAchievementsModel.prototype.changeData = function(summonerName, firsts) {
   this.firsts = firsts;
   this.firsts["summonerName"] = summonerName;

   if( this.callback )
      this.callback( this.firsts );
}

RecentAchievementsModel.prototype.getData = function() {
   return this.firsts;
}

RecentAchievementsModel.prototype.registerModelUpdatedCallback = function(callback) {
   this.callback = callback;
};

module.exports = RecentAchievementsModel;

},{}],8:[function(require,module,exports){
var TimelineModel = require("./TimelineModel.js");
var ObjectivesModel = require("./ObjectivesModel.js");

function RecentGamesModel( emitter ) {
   this.callback = null;
   this.timeline = {};
   this.emitter = emitter;

   this.recentGames = null;
   this.currentGame = 0;

   this.timelineModel = new TimelineModel(this.emitter);

   this.objectivesModel = new ObjectivesModel();

   this.changeGame = changeGame;
}

///
/// Public Methods
///

RecentGamesModel.prototype.getCurrentGame = function() {
   var currentGame = this.currentGame;

   if( !this.recentGames )
      return null;

   return this.recentGames[currentGame];
};

RecentGamesModel.prototype.changeData = function( summonerName, recentGames ) {
   this.recentGames = recentGames;
   this.currentGame = 0;
   var game = recentGames[0].gamedata;

   var currentGame = this.getCurrentGame();

   if( this.callback )
	   this.callback( currentGame );

   this.timelineModel.changeData(
      game.teamdata, 
      game.teamlinered,
      game.teamlineblue,
      game.teamredtotal,
      game.teambluetotal
   );

   this.objectivesModel.changeGame(
      game.objectives,
      game.gamelength
   );
};

RecentGamesModel.prototype.getTimelineModel = function() {
   return this.timelineModel;
};

RecentGamesModel.prototype.getObjectivesModel = function() {
   return this.objectivesModel;
};

RecentGamesModel.prototype.registerModelUpdatedCallback = function(callback) {
   this.callback = callback;
};

RecentGamesModel.prototype.nextGame = function() {
   var self = this;

   if( this.currentGame >= 14 )
      return;

   this.currentGame = this.currentGame + 1;

   this.changeGame();

   this.emitter.emit("NEXT_GAME_SHOWED", {
      "current_game": self.currentGame
   });
};

RecentGamesModel.prototype.previousGame = function() {
   var self = this;

   if( this.currentGame <= 0 )
      return;

   this.currentGame = this.currentGame - 1;

   this.changeGame();

   this.emitter.emit("PREVIOUS_GAME_SHOWED", {
      "current_game": self.currentGame
   });
};

RecentGamesModel.prototype.firstGame = function() {
   var self = this;
   this.currentGame = 0;
   this.changeGame();

   this.emitter.emit("FIRST_GAME_SHOWED", {
      "current_game": self.currentGame
   });
};

RecentGamesModel.prototype.lastGame = function() {
   var self = this;
   this.currentGame = 14;
   this.changeGame();

   this.emitter.emit("LAST_GAME_SHOWED", {
      "current_game": self.currentGame
   });
};


///
/// Private Methods
///

function changeGame() {
   var currentGame = this.getCurrentGame();

   if( this.callback )
      this.callback( currentGame );

   this.timelineModel.changeGame(
      currentGame.gamedata.teamdata,
      currentGame.gamedata.teamlinered,
      currentGame.gamedata.teamlineblue,
      currentGame.gamedata.teamredtotal,
      currentGame.gamedata.teambluetotal
      );

   this.objectivesModel.changeGame(
      currentGame.gamedata.objectives,
      currentGame.gamedata.gamelength
   );
};

module.exports = RecentGamesModel;

},{"./ObjectivesModel.js":5,"./TimelineModel.js":10}],9:[function(require,module,exports){
var BroadcasterModel = require("./BroadcasterModel.js");
var ChangeBroadcaster = require("./command/ChangeBroadcaster.js");
var ChangeTimeline = require("./command/ChangeTimeline.js");
var ShowFirstGame = require("./command/ShowFirstGame.js");
var ShowNextGame = require("./command/ShowNextGame.js");
var ShowPreviousGame = require("./command/ShowPreviousGame.js");
var ShowLastGame = require("./command/ShowLastGame.js");
var EmphasizeTime = require("./command/EmphasizeTime.js");
var events = require('events');

var instance = null;

function StatsFacade() {
   var self = this;

   this.emitter = new events.EventEmitter();
   this.events = [];

   this.broadcasterModel = new BroadcasterModel(this.emitter);
   this.performanceModel = this.broadcasterModel.getPerformanceModel();
   this.mostPlayedChampions = this.broadcasterModel.getMostPlayedChampionsModel();
   this.achievementsModel = this.broadcasterModel.getRecentAchievementsModel();
   this.multiKillHistory = this.broadcasterModel.getMultiKillHistoryModel();
   this.recentGameModel = this.broadcasterModel.getRecentGamesModel();
   this.timelineModel = this.recentGameModel.getTimelineModel();
	this.objectivesModel = this.recentGameModel.getObjectivesModel();

   this.emitter.on("BROADCASTER_CHANGED", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("NEXT_GAME_SHOWED", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("PREVIOUS_GAME_SHOWED", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("FIRST_GAME_SHOWED", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("LAST_GAME_SHOWED", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("LAST_GAME_SHOWED", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("TIMELINE_CHANGED", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("TIME_EMPHASIZED", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   function isoDate() {
      var isoDate = new Date().toISOString();
      return isoDate;
   }

   function guid() {
     function s4() {
       return Math.floor((1 + Math.random()) * 0x10000)
         .toString(16)
         .substring(1);
     }

     return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
       s4() + '-' + s4() + s4() + s4();
   }

   
}

StatsFacade.getInstance = function() {
	if( !instance )
		instance = new StatsFacade();
	
	return instance;
}

StatsFacade.prototype.getCommand = function( key ) {  
   if( key === "CHANGE_BROADCASTER" ) {
      var command = new ChangeBroadcaster( this.broadcasterModel );
      return command; 
   }

   if( key === "CHANGE_TIMELINE" ) {
      var model = this.recentGameModel.getTimelineModel();
      var command = new ChangeTimeline( model );
      return command;
   }

   if( key === "SHOW_FIRST_GAME" ) {
      var command = new ShowFirstGame( this.recentGameModel );
      return command;
   }

   if( key === "SHOW_NEXT_GAME" ) {
      var command = new ShowNextGame( this.recentGameModel );
      return command;
   }

   if( key === "SHOW_PREVIOUS_GAME" ) {
      var command = new ShowPreviousGame( this.recentGameModel );
      return command;
   }

   if( key === "SHOW_LAST_GAME" ) {
      var command = new ShowLastGame( this.recentGameModel );
      return command;
   }

   if( key === "EMPHASIZE_TIME" ) {
      var command = new EmphasizeTime( this.timelineModel )
      return command;
   }

   return null;
}

StatsFacade.prototype.addCallback = function( key, callback ) {
   this.emitter.on(key, callback);

   if( key === "PERFORMANCE_UPDATED" )
      this.performanceModel.registerModelUpdatedCallback( callback );
   if( key === "MOST_PLAYED_CHAMPIONS_UPDATED" )
      this.mostPlayedChampions.registerModelUpdatedCallback( callback );
   if( key === "ACHIEVEMENTS_UPDATED" )
      this.achievementsModel.registerModelUpdatedCallback( callback );
   if( key === "MULTI_KILL_HISTORY_UPDATED" )
      this.multiKillHistory.registerModelUpdatedCallback( callback );
   if( key === "RECENT_GAME_UPDATED" )
      this.recentGameModel.registerModelUpdatedCallback( callback );
   if( key === "TIMELINE_UPDATED" )
      this.timelineModel.registerModelUpdatedCallback( callback );
   if( key === "OBJECTIVES_UPDATED" )
      this.objectivesModel.registerModelUpdatedCallback( callback );
   if( key === "TIME_EMPHASIZED" )
      this.timelineModel.registerTimeEmphasizedCallback( callback );
}

StatsFacade.prototype.getEvents = function(since) {
   if( !since )
      return this.events;

   var filteredEvents = this.events.filter( function(evt) {
      return evt["event_time"] > since;
   });

   return filteredEvents;
}

module.exports = StatsFacade;

},{"./BroadcasterModel.js":2,"./command/ChangeBroadcaster.js":11,"./command/ChangeTimeline.js":12,"./command/EmphasizeTime.js":13,"./command/ShowFirstGame.js":14,"./command/ShowLastGame.js":15,"./command/ShowNextGame.js":16,"./command/ShowPreviousGame.js":17,"events":18}],10:[function(require,module,exports){
function TimelineModel( emitter ) {
   var self = this;
   this.teamData = null;
   this.emitter = emitter;
   this.hydrateData = hydrateData;

   //this.hydrateData( redTeam, blueTeam, redTeamTotal, blueTeamTotal );

   this.callback = null;
   this.state = "xpm";
   this.emphasizedTime = null;
   this.timeEmphasizedCallback = null;
}

TimelineModel.prototype.emphasizeTime = function( time ) {
   var self = this;
   this.emphasizedTime = time;
   this.emitter.emit( "TIME_EMPHASIZED", {
      "event_name": "time_emphasized",
      "state": self.state,
      "time": self.emphasizedTime
   });

   if( this.timeEmphasizedCallback )
      this.timeEmphasizedCallback( time );
}

TimelineModel.prototype.registerTimeEmphasizedCallback = function( callback ) {
   this.timeEmphasizedCallback = callback;
}

TimelineModel.prototype.changeData = function( 
   teamData, redTeam, blueTeam, redTeamTotal, blueTeamTotal ){

   this.teamData = teamData;
   this.hydrateData( redTeam, blueTeam, redTeamTotal, blueTeamTotal );
   //this.redTeam = redTeam;
   //this.blueTeam = blueTeam;
   //this.redTeamTotal = redTeamTotal;
   //this.blueTeamTotal = blueTeamTotal;
   this.emphasizedTime = null;

   if( this.callback )
      this.callback( {
         "state": this.state,
         "team_data": this.teamData,
         "red_team": this.redTeam,
         "blue_team": this.blueTeam
      });

}

TimelineModel.prototype.changeGame = function( 
   teamData, redTeam, blueTeam, redTeamTotal, blueTeamTotal ){

   this.teamData = teamData;
   this.hydrateData( redTeam, blueTeam, redTeamTotal, blueTeamTotal );

   if( this.callback )
      this.callback( {
         "state": this.state,
         "team_data": this.teamData,
         "red_team": this.redTeam,
         "blue_team": this.blueTeam
      });
};

TimelineModel.prototype.registerModelUpdatedCallback = function(callback) {
   this.callback = callback;
};

TimelineModel.prototype.changeChart = function( chart ) {
   var self = this;

   this.state = chart;
   this.emitter.emit("TIMELINE_CHANGED", {
      "state": self.state
   });

   if( this.callback )
      this.callback( {
         "state": this.state,
         "team_data": this.teamData,
         "red_team": this.redTeam,
         "blue_team": this.blueTeam
      });
};

TimelineModel.prototype.getData = function() {
   return {
      "state": this.state,
      "team_data": this.teamData,
      "red_team": this.redTeam,
      "blue_team": this.blueTeam
   };
};

///
/// Private Methods
///
function hydrateData( redTeam, blueTeam, redTeamTotal, blueTeamTotal) {
   var redData = {};
   var blueData = {};

   for (var attrname in redTeam) { 
      redData[attrname] = redTeam[attrname]; 
   }
   for (var attrname in redTeamTotal) { 
      redData[attrname] = redTeamTotal[attrname]; 
   }
   for (var attrname in blueTeamTotal) { 
      blueData[attrname] = blueTeamTotal[attrname]; 
   }
   for (var attrname in blueTeam) { 
      blueData[attrname] = blueTeam[attrname]; 
   }
   
   this.redTeam = redData;
   this.blueTeam = blueData;
}

module.exports = TimelineModel;

},{}],11:[function(require,module,exports){
function ChangeBroadcaster( broadcaster ) {
   this.broadcaster = broadcaster;
}

ChangeBroadcaster.prototype.execute = function(recentGames, medals, record ) {
   this.broadcaster.changeData( recentGames, medals, record );
}

module.exports = ChangeBroadcaster;

},{}],12:[function(require,module,exports){
function ChangeTimeline( model ) {
   this.model = model
}

ChangeTimeline.prototype.execute = function( newState ) {
   this.model.changeChart( newState );
}

module.exports = ChangeTimeline;

},{}],13:[function(require,module,exports){
function EmphasizeTime( timelineModel ) {
   this.model = timelineModel;
}

EmphasizeTime.prototype.execute = function( time ) {
   this.model.emphasizeTime( time );
}

module.exports = EmphasizeTime;

},{}],14:[function(require,module,exports){
function ShowFirstGame( recentGameModel ) {
   this.model = recentGameModel;
}

ShowFirstGame.prototype.execute = function() {
   this.model.firstGame();
}

module.exports = ShowFirstGame;

},{}],15:[function(require,module,exports){
function ShowLastGame( recentGameModel ) {
   this.model = recentGameModel;
}

ShowLastGame.prototype.execute = function() {
   this.model.lastGame();
}

module.exports = ShowLastGame;


},{}],16:[function(require,module,exports){
function ShowNextGame( recentGameModel ) {
   this.model = recentGameModel;
}

ShowNextGame.prototype.execute = function() {
   this.model.nextGame();
}

module.exports = ShowNextGame;

},{}],17:[function(require,module,exports){
function ShowPreviousGame( recentGameModel ) {
   this.model = recentGameModel;
}

ShowPreviousGame.prototype.execute = function() {
   this.model.previousGame();
}

module.exports = ShowPreviousGame;

},{}],18:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

function EventEmitter() {
  this._events = this._events || {};
  this._maxListeners = this._maxListeners || undefined;
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
EventEmitter.defaultMaxListeners = 10;

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function(n) {
  if (!isNumber(n) || n < 0 || isNaN(n))
    throw TypeError('n must be a positive number');
  this._maxListeners = n;
  return this;
};

EventEmitter.prototype.emit = function(type) {
  var er, handler, len, args, i, listeners;

  if (!this._events)
    this._events = {};

  // If there is no 'error' event listener then throw.
  if (type === 'error') {
    if (!this._events.error ||
        (isObject(this._events.error) && !this._events.error.length)) {
      er = arguments[1];
      if (er instanceof Error) {
        throw er; // Unhandled 'error' event
      }
      throw TypeError('Uncaught, unspecified "error" event.');
    }
  }

  handler = this._events[type];

  if (isUndefined(handler))
    return false;

  if (isFunction(handler)) {
    switch (arguments.length) {
      // fast cases
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      // slower
      default:
        len = arguments.length;
        args = new Array(len - 1);
        for (i = 1; i < len; i++)
          args[i - 1] = arguments[i];
        handler.apply(this, args);
    }
  } else if (isObject(handler)) {
    len = arguments.length;
    args = new Array(len - 1);
    for (i = 1; i < len; i++)
      args[i - 1] = arguments[i];

    listeners = handler.slice();
    len = listeners.length;
    for (i = 0; i < len; i++)
      listeners[i].apply(this, args);
  }

  return true;
};

EventEmitter.prototype.addListener = function(type, listener) {
  var m;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events)
    this._events = {};

  // To avoid recursion in the case that type === "newListener"! Before
  // adding it to the listeners, first emit "newListener".
  if (this._events.newListener)
    this.emit('newListener', type,
              isFunction(listener.listener) ?
              listener.listener : listener);

  if (!this._events[type])
    // Optimize the case of one listener. Don't need the extra array object.
    this._events[type] = listener;
  else if (isObject(this._events[type]))
    // If we've already got an array, just append.
    this._events[type].push(listener);
  else
    // Adding the second element, need to change to array.
    this._events[type] = [this._events[type], listener];

  // Check for listener leak
  if (isObject(this._events[type]) && !this._events[type].warned) {
    var m;
    if (!isUndefined(this._maxListeners)) {
      m = this._maxListeners;
    } else {
      m = EventEmitter.defaultMaxListeners;
    }

    if (m && m > 0 && this._events[type].length > m) {
      this._events[type].warned = true;
      console.error('(node) warning: possible EventEmitter memory ' +
                    'leak detected. %d listeners added. ' +
                    'Use emitter.setMaxListeners() to increase limit.',
                    this._events[type].length);
      if (typeof console.trace === 'function') {
        // not supported in IE 10
        console.trace();
      }
    }
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  var fired = false;

  function g() {
    this.removeListener(type, g);

    if (!fired) {
      fired = true;
      listener.apply(this, arguments);
    }
  }

  g.listener = listener;
  this.on(type, g);

  return this;
};

// emits a 'removeListener' event iff the listener was removed
EventEmitter.prototype.removeListener = function(type, listener) {
  var list, position, length, i;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events || !this._events[type])
    return this;

  list = this._events[type];
  length = list.length;
  position = -1;

  if (list === listener ||
      (isFunction(list.listener) && list.listener === listener)) {
    delete this._events[type];
    if (this._events.removeListener)
      this.emit('removeListener', type, listener);

  } else if (isObject(list)) {
    for (i = length; i-- > 0;) {
      if (list[i] === listener ||
          (list[i].listener && list[i].listener === listener)) {
        position = i;
        break;
      }
    }

    if (position < 0)
      return this;

    if (list.length === 1) {
      list.length = 0;
      delete this._events[type];
    } else {
      list.splice(position, 1);
    }

    if (this._events.removeListener)
      this.emit('removeListener', type, listener);
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  var key, listeners;

  if (!this._events)
    return this;

  // not listening for removeListener, no need to emit
  if (!this._events.removeListener) {
    if (arguments.length === 0)
      this._events = {};
    else if (this._events[type])
      delete this._events[type];
    return this;
  }

  // emit removeListener for all listeners on all events
  if (arguments.length === 0) {
    for (key in this._events) {
      if (key === 'removeListener') continue;
      this.removeAllListeners(key);
    }
    this.removeAllListeners('removeListener');
    this._events = {};
    return this;
  }

  listeners = this._events[type];

  if (isFunction(listeners)) {
    this.removeListener(type, listeners);
  } else {
    // LIFO order
    while (listeners.length)
      this.removeListener(type, listeners[listeners.length - 1]);
  }
  delete this._events[type];

  return this;
};

EventEmitter.prototype.listeners = function(type) {
  var ret;
  if (!this._events || !this._events[type])
    ret = [];
  else if (isFunction(this._events[type]))
    ret = [this._events[type]];
  else
    ret = this._events[type].slice();
  return ret;
};

EventEmitter.listenerCount = function(emitter, type) {
  var ret;
  if (!emitter._events || !emitter._events[type])
    ret = 0;
  else if (isFunction(emitter._events[type]))
    ret = 1;
  else
    ret = emitter._events[type].length;
  return ret;
};

function isFunction(arg) {
  return typeof arg === 'function';
}

function isNumber(arg) {
  return typeof arg === 'number';
}

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}

function isUndefined(arg) {
  return arg === void 0;
}

},{}]},{},[1])(1)
});