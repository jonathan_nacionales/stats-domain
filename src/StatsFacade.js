var BroadcasterModel = require("./model/BroadcasterModel.js");
var ChangeBroadcaster = require("./command/ChangeBroadcaster.js");
var ChangeTimeline = require("./command/ChangeTimeline.js");
var ShowFirstGame = require("./command/ShowFirstGame.js");
var ShowNextGame = require("./command/ShowNextGame.js");
var ShowPreviousGame = require("./command/ShowPreviousGame.js");
var ShowLastGame = require("./command/ShowLastGame.js");
var EmphasizeTime = require("./command/EmphasizeTime.js");
var events = require('events');

var instance = null;

function StatsFacade() {
   var self = this;
   this.events = [];

   var emitter = new events.EventEmitter();
   this.emitter = emitter;

   var model = new BroadcasterModel(emitter);
   var performanceModel = model.getPerformanceModel(emitter);
   var mostPlayedChampions = model.getMostPlayedChampionsModel(emitter);
   var achievementsModel = model.getRecentAchievementsModel(emitter);
   var multiKillHistory = model.getMultiKillHistoryModel(emitter);
   var recentGameModel = model.getRecentGamesModel(emitter);
   var timelineModel = recentGameModel.getTimelineModel(emitter);
   var objectivesModel = recentGameModel.getObjectivesModel(emitter);

   this.broadcasterModel = model;
   this.performanceModel = performanceModel;
   this.mostPlayedChampions = mostPlayedChampions;
   this.achievementsModel = achievementsModel;
   this.multiKillHistory = multiKillHistory; 
   this.recentGameModel = recentGameModel;
   this.timelineModel = timelineModel;
	this.objectivesModel = objectivesModel;

   this.emitter.on("BROADCASTER_CHANGED_EVENT", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("NEXT_GAME_SHOWED_EVENT", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("PREVIOUS_GAME_SHOWED_EVENT", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("FIRST_GAME_SHOWED_EVENT", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("LAST_GAME_SHOWED_EVENT", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("TIMELINE_CHANGED_EVENT", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   this.emitter.on("TIME_EMPHASIZED_EVENT", function(evt) {
      var uuid = self.broadcasterModel.getAggregateId(); 
      evt["event_id"] = guid();
      evt["aggregate_id"] = uuid;
      evt["event_time"] = isoDate();
      self.events.push(evt);
   });

   function isoDate() {
      var isoDate = new Date().toISOString();
      return isoDate;
   }

   function guid() {
     function s4() {
       return Math.floor((1 + Math.random()) * 0x10000)
         .toString(16)
         .substring(1);
     }

     return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
       s4() + '-' + s4() + s4() + s4();
   }
}

StatsFacade.getInstance = function() {
	if( !instance )
		instance = new StatsFacade();
	
	return instance;
}

StatsFacade.prototype.getCommand = function( key ) {  
   if( key === "CHANGE_BROADCASTER" ) {
      var command = new ChangeBroadcaster( this.broadcasterModel );
      return command; 
   }

   if( key === "CHANGE_TIMELINE" ) {
      var model = this.recentGameModel.getTimelineModel();
      var command = new ChangeTimeline( model );
      return command;
   }

   if( key === "SHOW_FIRST_GAME" ) {
      var command = new ShowFirstGame( this.recentGameModel );
      return command;
   }

   if( key === "SHOW_NEXT_GAME" ) {
      var command = new ShowNextGame( this.recentGameModel );
      return command;
   }

   if( key === "SHOW_PREVIOUS_GAME" ) {
      var command = new ShowPreviousGame( this.recentGameModel );
      return command;
   }

   if( key === "SHOW_LAST_GAME" ) {
      var command = new ShowLastGame( this.recentGameModel );
      return command;
   }

   if( key === "EMPHASIZE_TIME" ) {
      var command = new EmphasizeTime( this.timelineModel )
      return command;
   }

   return null;
}

StatsFacade.prototype.addCallback = function( key, callback ) {
   this.emitter.on(key, callback);
}

StatsFacade.prototype.getEvents = function(since) {
   if( !since )
      return this.events;

   var filteredEvents = this.events.filter( function(evt) {
      return evt["event_time"] > since;
   });

   return filteredEvents;
}

module.exports = StatsFacade;
