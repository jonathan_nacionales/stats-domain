function TimelineModel( emitter ) {
   var self = this;
   this.teamData = null;
   this.emitter = emitter;
   this.hydrateData = hydrateData;

   this.state = "xpm";
   this.emphasizedTime = null;
}

TimelineModel.prototype.emphasizeTime = function( time ) {
   var self = this;
   this.emphasizedTime = time;

   this.emitter.emit( "TIME_EMPHASIZED_EVENT", {
      "event_name": "time_emphasized",
      "state": self.state,
      "time": self.emphasizedTime
   });

   this.emitter.emit("TIME_EMPHASIZED", time);
}

TimelineModel.prototype.changeData = function( 
   teamData, redTeam, blueTeam, redTeamTotal, blueTeamTotal ){

   this.teamData = teamData;
   this.hydrateData( redTeam, blueTeam, redTeamTotal, blueTeamTotal );

   this.emphasizedTime = null;


   this.emitter.emit("TIMELINE_UPDATED", {
      "state": this.state,
      "team_data": this.teamData,
      "red_team": this.redTeam,
      "blue_team": this.blueTeam
   });
}

TimelineModel.prototype.changeGame = function( 
   teamData, redTeam, blueTeam, redTeamTotal, blueTeamTotal ){

   this.teamData = teamData;
   this.hydrateData( redTeam, blueTeam, redTeamTotal, blueTeamTotal );

   this.emitter.emit("TIMELINE_UPDATED", {
      "state": this.state,
      "team_data": this.teamData,
      "red_team": this.redTeam,
      "blue_team": this.blueTeam
   });
};

TimelineModel.prototype.changeChart = function( chart ) {
   var self = this;

   this.state = chart;
   this.emitter.emit("TIMELINE_CHANGED_EVENT", {
      "state": self.state
   });

   this.emitter.emit("TIMELINE_UPDATED", {
      "state": this.state,
      "team_data": this.teamData,
      "red_team": this.redTeam,
      "blue_team": this.blueTeam
   });
};

TimelineModel.prototype.getData = function() {
   return {
      "state": this.state,
      "team_data": this.teamData,
      "red_team": this.redTeam,
      "blue_team": this.blueTeam
   };
};

///
/// Private Methods
///
function hydrateData( redTeam, blueTeam, redTeamTotal, blueTeamTotal) {
   var redData = {};
   var blueData = {};

   for (var attrname in redTeam) { 
      redData[attrname] = redTeam[attrname]; 
   }
   for (var attrname in redTeamTotal) { 
      redData[attrname] = redTeamTotal[attrname]; 
   }
   for (var attrname in blueTeamTotal) { 
      blueData[attrname] = blueTeamTotal[attrname]; 
   }
   for (var attrname in blueTeam) { 
      blueData[attrname] = blueTeam[attrname]; 
   }
   
   this.redTeam = redData;
   this.blueTeam = blueData;
}

module.exports = TimelineModel;
