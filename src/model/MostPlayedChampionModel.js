function MostPlayedChampionModel(emitter) {
   this.emitter = emitter;
}

MostPlayedChampionModel.prototype.changeData = function(summonerName, topchamp) {
   this.topchamp = topchamp;
   this.topchamp["summonerName"] = summonerName;

   this.emitter.emit("MOST_PLAYED_CHAMPIONS_UPDATED", this.topchamp);
}

MostPlayedChampionModel.prototype.getData = function() {
   return this.topchamp;
}

module.exports = MostPlayedChampionModel;
