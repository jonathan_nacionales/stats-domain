function ObjectivesModel( emitter ) {
   this.emitter = emitter;
}

ObjectivesModel.prototype.changeData = function( data, timeLength ){

   this.data = data;
   this.timeLength = timeLength;

   this.emitter.emit("OBJECTIVES_UPDATED", {
      data: this.data,
      time_length: this.timeLength
   });
}

ObjectivesModel.prototype.changeGame = function( data, timeLength ){

   this.data = data;
   this.timeLength = timeLength;

   this.emitter.emit("OBJECTIVES_UPDATED", {
      data: this.data,
      time_length: this.timeLength
   });

}

ObjectivesModel.prototype.getData = function() {
   return {
      data: this.data,
      time_length: this.timeLength
   }
};

module.exports = ObjectivesModel;

