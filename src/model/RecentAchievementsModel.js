function RecentAchievementsModel(emitter) {
   this.emitter = emitter;
}

RecentAchievementsModel.prototype.changeData = function(summonerName, firsts) {
   this.firsts = firsts;
   this.firsts["summonerName"] = summonerName;

   this.emitter.emit("ACHIEVEMENTS_UPDATED", this.firsts );
}

RecentAchievementsModel.prototype.getData = function() {
   return this.firsts;
}

module.exports = RecentAchievementsModel;
