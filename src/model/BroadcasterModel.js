var RecentGamesModel = require("./RecentGamesModel.js");
var PerformanceModel = require("./PerformanceModel.js");
var MostPlayedChampionModel = require("./MostPlayedChampionModel.js");
var MultiKillHistoryModel = require("./MultiKillHistoryModel.js");
var RecentAchievementsModel = require("./RecentAchievementsModel.js");

function BroadcasterModel(emitter) {
   this.emitter = emitter;

   this.summonerName = "";

   this.medals = {};
   this.record = {};

   this.recentGames = new RecentGamesModel(emitter);
   this.performanceModel = new PerformanceModel(emitter);
   this.mostPlayedChampionsModel = new MostPlayedChampionModel(emitter);
   this.multiKillHistoryModel = new MultiKillHistoryModel(emitter);

   this.recentAchievementsModel = new RecentAchievementsModel(emitter);
}

BroadcasterModel.prototype.getPerformanceModel = function() {
   return this.performanceModel;
};

BroadcasterModel.prototype.getMostPlayedChampionsModel = function() {
   return this.mostPlayedChampionsModel;
};

BroadcasterModel.prototype.getMultiKillHistoryModel = function () {
   return this.multiKillHistoryModel;
};

BroadcasterModel.prototype.getRecentAchievementsModel = function() {
   return this.recentAchievementsModel;
};

BroadcasterModel.prototype.getRecentGamesModel = function() {
   return this.recentGames;
};

BroadcasterModel.prototype.changeData = function(recentGames, medals, record) {

   if( recentGames.game )
      recentGames = recentGames.game;

   this.summonerName = recentGames[0].summoner_name;
   var summonerName = this.summonerName;

   this.recentGames.changeData( summonerName, recentGames );
   this.performanceModel.changeData( summonerName, record );
   this.mostPlayedChampionsModel.changeData( summonerName, medals.topchamp );
   this.multiKillHistoryModel.changeData( summonerName, medals.multikill );
   this.recentAchievementsModel.changeData( summonerName, medals.firsts );

   this.uuid = guid();

   this.emitter.emit("BROADCASTER_CHANGED_EVENT", {
      "summoner_name": summonerName,
      "recent_games": recentGames,
      "medals": medals,
      "record": record
   })
};

BroadcasterModel.prototype.getAggregateId = function() {
   return this.uuid;
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

module.exports = BroadcasterModel;
