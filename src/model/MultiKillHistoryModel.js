function MultiKillHistoryModel(emitter) {
   this.emitter = emitter;
}

MultiKillHistoryModel.prototype.changeData = function(summonerName, multikill) {
   this.multikill = multikill;
   this.multikill["summonerName"] = summonerName;

   this.emitter.emit("MULTI_KILL_HISTORY_UPDATED", this.multikill);
}

MultiKillHistoryModel.prototype.getData = function() {
   return this.multikill;
}

module.exports = MultiKillHistoryModel;
