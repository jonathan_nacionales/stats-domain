function PerformanceModel(emitter) {
   this.emitter = emitter;
}

PerformanceModel.prototype.changeData = function(summonerName, record) {
   this.record = record;
   this.record["summonerName"] = summonerName;

   this.emitter.emit( "PERFORMANCE_UPDATED", this.record );
}

PerformanceModel.prototype.getData = function() {
   return this.record;
}

module.exports = PerformanceModel;
