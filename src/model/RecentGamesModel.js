var TimelineModel = require("./TimelineModel.js");
var ObjectivesModel = require("./ObjectivesModel.js");

function RecentGamesModel( emitter ) {
   this.timeline = {};
   this.emitter = emitter;

   this.recentGames = null;
   this.currentGame = 0;

   this.timelineModel = new TimelineModel(emitter);
   this.objectivesModel = new ObjectivesModel(emitter);

   this.changeGame = changeGame;
}

///
/// Public Methods
///

RecentGamesModel.prototype.getCurrentGame = function() {
   var currentGame = this.currentGame;

   if( !this.recentGames )
      return null;

   return this.recentGames[currentGame];
};

RecentGamesModel.prototype.changeData = function( summonerName, recentGames ) {
   this.recentGames = recentGames;
   this.currentGame = 0;
   var game = recentGames[0].gamedata;

   var currentGame = this.getCurrentGame();

   this.emitter.emit("RECENT_GAME_UPDATED", currentGame );

   this.timelineModel.changeData(
      game.teamdata, 
      game.teamlinered,
      game.teamlineblue,
      game.teamredtotal,
      game.teambluetotal
   );

   this.objectivesModel.changeGame(
      game.objectives,
      game.gamelength
   );
};

RecentGamesModel.prototype.getTimelineModel = function() {
   return this.timelineModel;
};

RecentGamesModel.prototype.getObjectivesModel = function() {
   return this.objectivesModel;
};

RecentGamesModel.prototype.nextGame = function() {
   var self = this;

   if( this.currentGame >= 14 )
      return;

   this.currentGame = this.currentGame + 1;

   this.changeGame();

   this.emitter.emit("NEXT_GAME_SHOWED_EVENT", {
      "current_game": self.currentGame
   });
};

RecentGamesModel.prototype.previousGame = function() {
   var self = this;

   if( this.currentGame <= 0 )
      return;

   this.currentGame = this.currentGame - 1;

   this.changeGame();

   this.emitter.emit("PREVIOUS_GAME_SHOWED_EVENT", {
      "current_game": self.currentGame
   });
};

RecentGamesModel.prototype.firstGame = function() {
   var self = this;
   this.currentGame = 0;
   this.changeGame();

   this.emitter.emit("FIRST_GAME_SHOWED_EVENT", {
      "current_game": self.currentGame
   });
};

RecentGamesModel.prototype.lastGame = function() {
   var self = this;
   this.currentGame = 14;
   this.changeGame();

   this.emitter.emit("LAST_GAME_SHOWED_EVENT", {
      "current_game": self.currentGame
   });
};


///
/// Private Methods
///

function changeGame() {
   var currentGame = this.getCurrentGame();

   this.emitter.emit("RECENT_GAME_UPDATED", currentGame );

   this.timelineModel.changeGame(
      currentGame.gamedata.teamdata,
      currentGame.gamedata.teamlinered,
      currentGame.gamedata.teamlineblue,
      currentGame.gamedata.teamredtotal,
      currentGame.gamedata.teambluetotal
      );

   this.objectivesModel.changeGame(
      currentGame.gamedata.objectives,
      currentGame.gamedata.gamelength
   );
};

module.exports = RecentGamesModel;
