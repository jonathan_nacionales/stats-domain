function ShowPreviousGame( recentGameModel ) {
   this.model = recentGameModel;
}

ShowPreviousGame.prototype.execute = function() {
   this.model.previousGame();
}

module.exports = ShowPreviousGame;
