function ChangeBroadcaster( broadcaster ) {
   this.broadcaster = broadcaster;
}

ChangeBroadcaster.prototype.execute = function(recentGames, medals, record ) {
   this.broadcaster.changeData( recentGames, medals, record );
}

module.exports = ChangeBroadcaster;
