function ShowNextGame( recentGameModel ) {
   this.model = recentGameModel;
}

ShowNextGame.prototype.execute = function() {
   this.model.nextGame();
}

module.exports = ShowNextGame;
