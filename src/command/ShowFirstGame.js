function ShowFirstGame( recentGameModel ) {
   this.model = recentGameModel;
}

ShowFirstGame.prototype.execute = function() {
   this.model.firstGame();
}

module.exports = ShowFirstGame;
