function EmphasizeTime( timelineModel ) {
   this.model = timelineModel;
}

EmphasizeTime.prototype.execute = function( time ) {
   this.model.emphasizeTime( time );
}

module.exports = EmphasizeTime;
