function ShowLastGame( recentGameModel ) {
   this.model = recentGameModel;
}

ShowLastGame.prototype.execute = function() {
   this.model.lastGame();
}

module.exports = ShowLastGame;

