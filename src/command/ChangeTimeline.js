function ChangeTimeline( model ) {
   this.model = model
}

ChangeTimeline.prototype.execute = function( newState ) {
   this.model.changeChart( newState );
}

module.exports = ChangeTimeline;
